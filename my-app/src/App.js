import './App.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import About from './pages/about';
import Home from './pages/home';
import Project from './pages/project';
import Story from './pages/story';
import Nav from './components/navbar/nav';
import Footer from './components/footer';

function App() {
  return (
    <Router>
      <div className="App">
        <div className="content">
          <Nav />
          <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/aboutme' component={About} />
          <Route path='/story' component={Story} />
          <Route path='/project' component={Project} />
          </Switch>
        </div>
        <div className="footer1">
          <Footer />
        </div>
      </div>
    </Router>
  );
}

export default App;
