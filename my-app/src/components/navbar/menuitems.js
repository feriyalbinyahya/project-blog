export const MenuItems = [
    {
        title: "Project",
        url: '/project',
        cName: 'nav-links'
    },
    {
        title: "Story",
        url: '/story',
        cName: 'nav-links'
    },
    {
        title: "About me",
        url: '/aboutme',
        cName: 'nav-links'
    }
];