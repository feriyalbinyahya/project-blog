import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar } from 'react-bootstrap';
import {MenuItems} from './menuitems';
import './nav.css';

function Nav() {
    return (
        <Navbar className="NavbarItems">
            <Navbar.Brand href="/" id="navbar-logo">Feriyal's Land</Navbar.Brand>
            <ul className="nav-menu">
                {MenuItems.map((item, index) => {
                    return (
                        <li key={index}>
                            <a className={item.cName} href={item.url}>{item.title}</a>
                        </li>
                    );
                })}
            </ul>
        </Navbar>
    );
}

export default Nav;