import React from 'react';
import './footer.css';
import 'bootstrap/dist/css/bootstrap.min.css';


function Footer() {
    return (
        <div class="row-footer">
            <p className="copyright">&copy; Feriyal</p>
        </div>
    );
}

export default Footer;