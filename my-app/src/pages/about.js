import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Feri from '../assets/aboutme/feri.jpg';
import Button from 'react-bootstrap/Button';
import ProgressBar from 'react-bootstrap/ProgressBar';
import MyCV from '../assets/aboutme/mycv.pdf';
import { FaInstagram } from "react-icons/fa";
import { FaLinkedinIn } from "react-icons/fa";
import { FaFacebookF } from "react-icons/fa";
import PythonImg from '../assets/aboutme/python.jpg';
import JavaImg from '../assets/aboutme/java.jpg';
import Html5Img from '../assets/aboutme/html5.png';
import Css3Img from '../assets/aboutme/css3.png';
import DjangoImg from '../assets/aboutme/django.png';
import FlutterImg from '../assets/aboutme/flutter.png';
import JavascriptImg from '../assets/aboutme/javascript.jpg';
import PhpImg from '../assets/aboutme/php.png';
import ReactImg from '../assets/aboutme/react.png';
import './about.css'

function About () {
    return (
        <div style={{height:1000}}>
            <Container>
                <Row className="about-me-title">
                    <h2>My Profile</h2>
                </Row>
                <Row className="row-box-about">
                    <div className="box-about">
                        <Container>
                            <Row>
                                <Col sm={3}>
                                    <Row>
                                        <img src={Feri} className="img-feri" />
                                    </Row>
                                    <Row>
                                        <div className="div-button">
                                            <Button href={MyCV} id="button-resume">My Resume</Button>
                                        </div>
                                    </Row>
                                </Col>
                                <Col sm={8}>
                                    <Row>
                                        <p className="my-profile">Hi, my name is Firriyal Bin Yahya, but you can call me Feriyal. I am 20 years old. I am currently demanding knowledge about the world of IT. 
                                            I am also studying several computer languages, such as Python, Java, HTML5, CSS3, 
                                            Javascript. Besides that, I am someone who is responsible and wants to learn more things.</p>
                                    </Row>
                                    <Row>
                                        <div className="sosmed">
                                                <a href="https://www.instagram.com/feriyalbinyahya/"><FaInstagram /></a>
                                                <a href="https://www.linkedin.com/in/feriyalbinyahya/"><FaLinkedinIn /></a>
                                                <a href="https://m.facebook.com/fridai17?ref=wizard"><FaFacebookF /></a>
                                        </div>
                                    </Row> 
                                </Col>
                            </Row>
                        </Container>
                    </div>
                </Row>
                <Row className="skills-title">
                    <h2>My Skills</h2>
                </Row>
                <Row>
                    <Container>
                        <Row className="row-skills-1">
                            <Col sm={2}>
                                <img src={PythonImg} className="python-img" />
                                <img src={DjangoImg} className="django-img" />
                                <img src={JavascriptImg} className="js-img" />
                            </Col>
                            <Col sm={2}> 
                                <img src={JavaImg} className="java-img" />
                                <img src={Css3Img} className="css-img" />
                                <img src={PhpImg} className="css-img" />
                            </Col>
                            <Col sm={2}>
                                <img src={Html5Img} className="html-img" />
                                <img src={FlutterImg} className="flutter-img" />
                                <img src={ReactImg} className="react-img" />
                            </Col>
                        </Row>
                        
                        <Row className="row-education">

                        </Row>
                    </Container>
                </Row>
            </Container>
        </div>
    );
}

export default About;