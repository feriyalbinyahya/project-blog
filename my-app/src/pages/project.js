import './project.css';
import React from 'react';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Container from 'react-bootstrap/Container'
import PinkAndBlue from '../assets/project/pinkandblue.png'
import Landai from '../assets/project/landai.png'
import BisaGo from '../assets/project/bisago.png'
import SinarPerak from '../assets/project/sinarperak.png'
import LineBot from '../assets/project/line.png'

function Project () {
    return (
        <div style={{width:1500}}>
            <br />
            <Container>
                <Row>
                    <Col sm={11}><h2 id="my-project">My Project</h2></Col>
                </Row>
            </Container>
            <Container>
                <Row>
                    <Col sm={6}>
                        <Container className="box-project-1">
                            <div className="project-1">
                                <Row>
                                    <h4 className="title-project-1">Pink and Blue - Game</h4>
                                </Row>
                                <Row style={{height: 150}}><p>Pink and Blue is a 2D arcade racing game played by two players. 
                                    In this game there are five levels with different challenges for players to pass. 
                                    This game was made using the Godot Engine in the framework of the CSUI Individual 
                                    Game Jam of 2020 project.
                                </p></Row>
                                <a href="https://feriyalbinyahya.itch.io/pink-and-blue"><p style={{color: 'black'}}>Check this out...</p></a>
                            </div>
                        </Container>
                    </Col>
                    <Col sm={6}>
                        <Container className="img-box-1">
                            <img src={PinkAndBlue} className="img-pinkandblue" />
                        </Container>
                    </Col>
                </Row>
            </Container>
            <Row style={{height: 150}}></Row>
            <Container>
                <Row>
                    <Col sm={6}>
                        <Container className="img-box-1">
                            <img src={Landai} className="img-landai" />
                        </Container>
                    </Col>
                    <Col sm={6}>
                        <Container className="box-project-2">
                            <div className="project-1">
                                <Row>
                                    <h4 className="title-project-1">Landai - UI/UX Mobile App</h4>
                                </Row>
                                <Row style={{height: 150}}><p>Designing UI / UX for applications that can help people in conditions 
                                    of the COVID-19 pandemic. The application can detect the distance and health of people those around them,
                                     as well as provide notifications to them.
                                </p></Row>
                                <a href="https://www.figma.com/file/DztAylb7An6E8dqiBsyF0d/Landai-Real?node-id=0%3A1"><p style={{color: 'black'}}>Check this out...</p></a>
                            </div>
                        </Container>
                    </Col>
                </Row>
            </Container>
            <Row style={{height: 150}}></Row>
            <Container>
                <Row>
                    <Col sm={6}>
                        <Container className="box-project-1">
                            <div className="project-1">
                                <Row>
                                    <h4 className="title-project-1">BisaGo - Mobile App</h4>
                                </Row>
                                <Row style={{height: 150}}><p>BisaGo is an mobile app to help disabilities person in finding information and 
                                    facilities for them. This app created using Flutter framework. Collaborate with group members.
                                </p></Row>
                                <a href="https://apkpure.com/id/bisago/com.ppl.bisaGo"><p style={{color: 'black'}}>Check this out...</p></a>
                            </div>
                        </Container>
                    </Col>
                    <Col sm={6}>
                        <Container className="img-box-1">
                            <img src={BisaGo} className="img-pinkandblue" />
                        </Container>
                    </Col>
                </Row>
            </Container>
            <Row style={{height: 150}}></Row>
            <Container>
                <Row>
                    <Col sm={6}>
                        <Container className="img-box-1">
                            <img src={SinarPerak} className="img-sinarperak" />
                        </Container>
                    </Col>
                    <Col sm={6}>
                        <Container className="box-project-2">
                            <div className="project-1">
                                <Row>
                                    <h4 className="title-project-1">Sinar Perak - Web App</h4>
                                </Row>
                                <Row style={{height: 150}}><p>The website to helps donations for others. Created using Python, and Django framework. 
                                    Collaborate with group members.
                                </p></Row>
                            </div>
                        </Container>
                    </Col>
                </Row>
            </Container>
            <Row style={{height: 150}}></Row>
            <Container>
                <Row>
                    <Col sm={6}>
                        <Container className="box-project-1">
                            <div className="project-1">
                                <Row>
                                    <h4 className="title-project-1">Sebastian Butler Bot - Line Bot</h4>
                                </Row>
                                <Row style={{height: 150}}><p>Sebastian Butler Bot is a LINE bot created using Java and Spring framework. This bot helps 
                                    you to know about Indonesia language things. Collaborate with group members.
                                </p></Row>
                            </div>
                        </Container>
                    </Col>
                    <Col sm={6}>
                        <Container className="img-box-1">
                            <img src={LineBot} className="img-line" />
                        </Container>
                    </Col>
                </Row>
            </Container>
            <Row style={{height: 150}}></Row>
        </div>
    );
}

export default Project;